//
//  gonettunesTests.swift
//  gonettunesTests
//
//  Created by Misael Pérez Chamorro on 4/19/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import XCTest
@testable import gonettunes

class gonettunesTests: XCTestCase {
    
  override func setUp() {
    super.setUp()
    let session = Session.sharedInstance
    session.token = nil
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testCorrectLogin() {
    XCTAssertTrue(User.login(userName: "JohnGonet", password: ""))
  }

  func testWrongLogin() {
    XCTAssertFalse(User.login(userName: "Raul", password: ""))
  }
  
  func testSaveSession() {
    let session = Session.sharedInstance
    let _ = User.login(userName: "JohnGonet", password: "223")
    XCTAssertNotNil(session.token)
  }
  
  func testFailSaveSession() {
    let session = Session.sharedInstance
    let _ = User.login(userName: "Jorge", password: "223")
    XCTAssertNil(session.token)
  }
  
  func testExpectedToken() {
    let _ = User.login(userName: "JohnGonet", password: "223")
    let session = Session.sharedInstance
    XCTAssertEqual(session.token!, "1234567890","Token Should Match")
  }
  
  func testGetErrorWhenNoloogedIn() {
    XCTAssertThrowsError(try User.fetchSongs())
  }
  
  func testMusicSongs() {
    var resultSongs: [Song] = []
    let promise = expectation(description: "Songs Downloaded")
    Music.fetchSongs { (songs) in
      resultSongs = songs
      promise.fulfill()
    }
    waitForExpectations(timeout: 5, handler: nil)
    XCTAssertNotEqual(resultSongs.count, 0)
  }
}
