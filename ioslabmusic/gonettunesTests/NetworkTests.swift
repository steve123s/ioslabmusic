//
//  NetworkTests.swift
//  gonettunesTests
//
//  Created by Misael Pérez Chamorro on 4/19/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import XCTest

@testable import gonettunes

class NetworkTests: XCTestCase {
  var sessionUnderTest: URLSession!
  
  override func setUp() {
    super.setUp()
    sessionUnderTest = URLSession(configuration: .default)
  }
  
  override func tearDown() {
    
  }
  
  func testSlowValidCallToItunes() {
    let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=abba")
    let promise = expectation(description: "Status code: 200")
    let dataTask = sessionUnderTest.dataTask(with: url!) {data, response, error in
      if let error = error {
        XCTFail("Error: (\(error.localizedDescription)")
      } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
        if statusCode == 200 {
          promise.fulfill()
        } else {
          XCTFail("Status code: \(statusCode)")
        }
      }
    }
    dataTask.resume()
    waitForExpectations(timeout: 5, handler: nil)
  }
  
  func testValidCallToItunes() {
    let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=abba")
    let promise = expectation(description: "Handler Invoked")
    var statusCode: Int?
    var responseError: Error?
    
    let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
      statusCode = (response as? HTTPURLResponse)?.statusCode
      responseError = error
      promise.fulfill()
    }
    dataTask.resume()
    waitForExpectations(timeout: 5, handler: nil)
    XCTAssertNil(responseError)
    XCTAssertEqual(statusCode, 200)
  }
}

