//
//  MusicViewController.swift
//  gonettunes
//
//  Created by Misael Pérez Chamorro on 4/19/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController {
  var songs:[Song] = []
  let searchController = UISearchController(searchResultsController: nil)

  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    tableView.accessibilityLabel = "table--articleTableView"
//    tableView.isAccessibilityElement = true
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Search Songs"
    
    navigationItem.searchController = searchController
    definesPresentationContext = true
    Music.fetchSongs { (results: [Song]) in
      DispatchQueue.main.sync {
        self.songs = results
        self.tableView.reloadData()
      }
    }
    // Do any additional setup after loading the view.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func searchBarIsEmpty() -> Bool {
    // Returns true if the text is empty or nil
    return searchController.searchBar.text?.isEmpty ?? true
  }
  
  /*
  // MARK: - Navigation

  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
  }
  */
}

extension MusicViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return songs.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "MusicCell", for: indexPath)
    cell.textLabel?.text = songs[indexPath.row].artist
    return cell
  }
}

extension MusicViewController: UISearchResultsUpdating {
  // MARK: - UISearchResultsUpdating Delegate
  func updateSearchResults(for searchController: UISearchController) {
    Music.fetchSongs(songName: searchController.searchBar.text!) { (results: [Song]) in
      DispatchQueue.main.sync {
        self.songs = results
        self.tableView.reloadData()
      }
    }
  }
}
