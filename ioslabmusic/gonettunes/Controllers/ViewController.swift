//
//  ViewController.swift
//  gonettunes
//
//  Created by Misael Pérez Chamorro on 4/19/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  @IBAction func loginButtonWasTouchedUpInside(sender: Any) {
    let username = nameTextField.text
    let password = passwordTextField.text
    if User.login(userName: username!, password: password!) {
      performSegue(withIdentifier: "ShowMusic", sender: self)
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

