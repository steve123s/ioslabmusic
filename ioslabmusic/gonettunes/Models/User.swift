//
//  User.swift
//  gonettunes
//
//  Created by Misael Pérez Chamorro on 4/19/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import Foundation


class User {
  static let userName = "JohnGonet"
  static let password = "verySecurePassword"
  static let session = Session.sharedInstance
  
  static func login(userName: String, password: String) -> Bool{
    if self.userName == userName {
      session.saveSession()
      return true
    }
    return false
  }
  
  static func fetchSongs() throws {
    guard let token = Session.sharedInstance.token else {
      throw UserError.notSessionAvailable
    }
    debugPrint(token)
  }
}
enum UserError: Error {
  case notSessionAvailable
}
